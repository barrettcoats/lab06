import java.util.Scanner;

public class Main {

  //Tier 2: Check multiple-word phrases to determine if they are a palindrome.
  //Assume words are both upper and lower case and disregard all punctuation.
  public static void main(String[] args) {

    Scanner input = new Scanner(System.in);

    System.out.println();
    System.out.println("Palindrome Check Program");
    System.out.println("Please input a phrase.");
    String string = input.nextLine();

    isPalindromeStringBuilder(string);
  }

  public static boolean isPalindromeStringChar(String string) {

    int left = 0;
    int right = string.length() - 1;
    int badChar = 0;
    boolean offChar = false;

    while (left < right) {
      while (!Character.isAlphabetic(string.charAt(left))) {
        left++;
      }

      while (!Character.isAlphabetic(string.charAt(right))) {
        right--;
      }

      boolean charMismatch = Character.toLowerCase(string.charAt(left)) != Character.toLowerCase(string.charAt(right));
      if (charMismatch && offChar) {
        return false;
      } else if (charMismatch) {

        int tempLeft = left + 1;
        int tempRight = right - 1;

        while (!Character.isAlphabetic(string.charAt(tempLeft))) {
          tempLeft++;
        }
        while (!Character.isAlphabetic(string.charAt(tempRight))) {
          tempRight--;
        }

        if (Character.toLowerCase(string.charAt(tempLeft)) == Character.toLowerCase(string.charAt(right))) {
          badChar = left;
          left = tempLeft;
          offChar = true;
        } else if (Character.toLowerCase(string.charAt(tempRight)) == Character.toLowerCase(string.charAt(left))) {
          badChar = right;
          right = tempRight;
          offChar = true;
        } else {
          System.out.println("Removing a single character will not yield a palindrome");
          return false;
        }


      }

      left++;
      right--;
    }

    if (offChar) {
      System.out.println(" ".repeat(badChar) + "^");
      return false;
    }

    return true;

  }

  public static void isPalindromeStringBuilder(String string) {

    for (int i = -1; i < string.length(); i++) {

      StringBuilder stringBuilder = new StringBuilder();

      //Remove non-alphabetic chars from the string builder and the current char we are testing to
      //see if it results in a palindrome.
      for (int j = 0; j < string.length(); j++) {
        if (Character.isAlphabetic(string.charAt(j)) && i != j) {
          stringBuilder.append(string.charAt(j));
        }
      }

      String cleaned = stringBuilder.toString().toLowerCase();
      stringBuilder.reverse();
      String reversed = stringBuilder.toString().toLowerCase();

      if (reversed.equals(cleaned) && i == -1) {
        System.out.println("\nStringBuilder Check: is a palindrome.\n");
      } else if (reversed.equals(cleaned)) {
        System.out.println("\nStringBuilder Check: is a palindrome. (OFF BY ONE)\n");
      }

    }
    System.out.println("IS NOT");
  }
}

